=== WooCommerce Shmart Gateway ===
Contributors: kdclabs, vachan
Donate link: https://www.payumoney.com/webfront/index/kdclabs
Tags: WooCommerce, Payment Gateway, Shmart, Online payment in India
Requires at least: 3.5.1
Tested up to: 4.3.1
Stable tag: 1.0.0
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Online Payments for India. Shmart is the simplest way to collect payments online. Extends WooCommerce with an Shmart gateway.

== Description ==

WooCommerce - The most user friendly e-commerce toolkit that helps you sell anything. Beautifully.
[Shmart](https://www.shmart.in "Get your free account") - Online Payments for India. Shmart is the simplest way to collect payments online.

The two together is an awsome combination for any INDIAN merchant looking for an eCommerece pressence, without any finiacial load.
*   "WooCommerce" is an open source application
*   "Shmart" is an online payment gateway which allows Indian businesses to collect payments online.

Visit [www.kdcLabs.com](http://www.kdclabs.com/?p=181 "_KDC-Labs : ") for more info about this plugin.


== Installation ==

1. Ensure you have latest version of WooCommerce plugin installed (WooCommerce 2.4+)
2. Unzip and upload contents of the plugin to your `/wp-content/plugins/` directory
3. Activate the plugin through the 'Plugins' menu in WordPress

== Configuration ==

1. Visit the `WooCommerce > Settings > Checkout` tab.
2. Click on *Shmart* to edit the settings. If you do not see *Shmart* in the list at the top of the screen make sure you have activated the plugin in the WordPress Plugin Manager.
3. Enable the Payment Method, name it `Credit Card / Debit Card / Internet Banking` (this will show up on the payment page your customer sees).
4. Add in your `Key ID` and `Key Secrect` as generated in the Shmart Dashboard.
5. Choose if you want to show the `Shmart` Logo to the customer (You may also insert a custom logo in your discription via `<img ...` tag).
6. Select `Redirect url` (URL you want Shmart to redirect after payment).
7. Click Save.

== Screenshots ==

1. WooCommerce > Payment Gateway > Shmart - setting page
2. Checkout Page - Option of Payment by *Shmart*
3. Shmart - Payment slection page
4. Return Page - Transaction status as per *Shmart* display on return page as per setting

== Changelog ==

= 1.0.0 =
2015-10-14
* First Public Release.
